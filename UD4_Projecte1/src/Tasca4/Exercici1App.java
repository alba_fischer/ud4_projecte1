package Tasca4;

public class Exercici1App {

	public static void main(String[] args) {
		/* Declarem dues variables num�riques i mostrem per
		 * consola la suma, resta, multiplicaci�, divisi� i m�dul 
		 */
		int a = 10, b = 5, suma = a + b, resta = a - b, mult = a * b, div = a / b, mod = a % b  ;

		System.out.println("La suma de les dues variables �s: " + suma);
		System.out.println("La resta de les dues variables �s: " + resta);
		System.out.println("La multiplicaci� de les dues variables �s: " + mult);
		System.out.println("La divisi� de les dues variables �s: " + div);
		System.out.println("El m�dul de les dues variables �s: " + mod);
		
	}

}
